# Copyright 2008-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2020 Ali Polatel <alip@exherbo.org>
# Based in part upon previous work copyrighted to Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Collection of simple PIN or passphrase entry dialogs which utilize"
HOMEPAGE="https://gnupg.org/related_software/${PN}"
DOWNLOADS="mirror://gnupg/${PN}/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    emacs
    gnome
    gtk
    keyring [[ description = [ Cache passphrases using libsecret ] ]]
    qt5
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libassuan[>=2.1.0]
        dev-libs/libgpg-error[>=1.16]
        sys-libs/ncurses
        gnome? ( gnome-desktop/gcr:0 )
        gtk? ( x11-libs/gtk+:2[>=2.12.0] )
        keyring? ( dev-libs/libsecret:1 )
        qt5? (
            kde-frameworks/kwayland:5[>=5.60]
            x11-libs/libX11
            x11-libs/qtbase:5[>=5.7.0][gui]
            x11-libs/qtx11extras:5[>=5.1.0]
        )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-doc
    --enable-maintainer-mode
    --enable-fallback-curses
    --enable-pinentry-curses
    --enable-pinentry-tty
    --disable-pinentry-efl
    --disable-pinentry-fltk
    --disable-pinentry-qt4
    --disable-pinentry-tqt
    --with-libassuan-prefix=/usr/$(exhost --target)
    --with-libgpg-error-prefix=/usr/$(exhost --target)
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "emacs inside-emacs"
    "emacs pinentry-emacs"
    "gnome pinentry-gnome3"
    "gtk pinentry-gtk2"
    "keyring libsecret"
    "qt5 pinentry-qt"
    "qt5 pinentry-qt5"
)

DEFAULT_SRC_COMPILE_PARAMS=( AR="$(exhost --tool-prefix)ar" )


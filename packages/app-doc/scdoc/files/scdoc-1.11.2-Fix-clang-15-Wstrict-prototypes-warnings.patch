Upstream: yes
Reason: Fix build with clang >= 16

From 1923fb61cd71182f05d029f280d056b21f66e8a0 Mon Sep 17 00:00:00 2001
From: Dimitry Andric <dimitry@andric.com>
Date: Fri, 9 Dec 2022 17:59:32 +0100
Subject: [PATCH] Fix clang 15 -Wstrict-prototypes warnings

As of https://github.com/llvm/llvm-project/commit/11da1b53d8c, clang 15
has started warning about functions which take no arguments, but are
declared or defined using "()" instead of "(void)". See also [1].

At first this was even an error by default, but before clang 15 was
released, it was again downgraded to a warning. Since scdoc builds
with both "-std=c99 -pedantic" and "-Wall -Wextra -Werror", this leads
to two compile errors:

    include/str.h:10:23: error: a function declaration without a prototype is deprecated in all versions of C [-Werror,-Wstrict-prototypes]
    struct str *str_create();
                          ^
                           void
    src/string.c:15:23: error: a function declaration without a prototype is deprecated in all versions of C [-Werror,-Wstrict-prototypes]
    struct str *str_create() {
                          ^
                           void

To fix this, use "(void)" for both the declaration and definition of the
str_create function.

[1] https://discourse.llvm.org/t/rfc-enabling-wstrict-prototypes-by-default-in-c/60521
---
 include/str.h | 2 +-
 src/string.c  | 2 +-
 2 files changed, 2 insertions(+), 2 deletions(-)

diff --git a/include/str.h b/include/str.h
index ab7ba69..c796b78 100644
--- a/include/str.h
+++ b/include/str.h
@@ -7,7 +7,7 @@ struct str {
 	size_t len, size;
 };
 
-struct str *str_create();
+struct str *str_create(void);
 void str_free(struct str *str);
 void str_reset(struct str *str);
 int str_append_ch(struct str *str, uint32_t ch);
diff --git a/src/string.c b/src/string.c
index 2fbacbe..da605ce 100644
--- a/src/string.c
+++ b/src/string.c
@@ -12,7 +12,7 @@ static void ensure_capacity(struct str *str, size_t len) {
 	}
 }
 
-struct str *str_create() {
+struct str *str_create(void) {
 	struct str *str = xcalloc(1, sizeof(struct str));
 	str->str = xcalloc(16, 1);
 	str->size = 16;
-- 
2.41.0


# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pinfo-0.6.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require github [ user=baszoetekouw tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Hypertext info and man viewer based on (n)curses"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="readline"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/gettext[>=0.14.4]
        virtual/pkg-config
    build+run:
        sys-libs/ncurses
        readline? ( sys-libs/readline:= )
"

AT_M4DIR=( macros )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-workaround-false-positie-in-gcc-10-stringop-overflow.patch
    "${FILES}"/${PNV}-fix-use-of-global-variabel.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --with-ncurses
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( readline )

src_prepare() {
    default

    # Besides future changes causing errors, due to
    # https://github.com/baszoetekouw/pinfo/issues/10
    edo sed -e "s/ -Werror//" -i configure.ac

    # eautoreconf fails with "error: possibly undefined macro: AM_INTL_SUBDIR"
    autotools_select_versions
    edo ./autogen.sh
}


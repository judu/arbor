# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_install

SUMMARY="PoDoFo is a library to work with the PDF file format"
DESCRIPTION="
The PoDoFo library is a free, portable C++ library which includes classes to parse
PDF files and modify their contents into memory. The changes can be written back
to disk easily. The parser can also be used to extract information from a PDF file
(for example the parser could be used in a PDF viewer). Besides parsing PoDoFo
includes also very simple classes to create your own PDF files. All classes are
documented so it is easy to start writing your own application using PoDoFo.
"

LICENCES="GPL-2 LGPL-2"
MYOPTIONS="
    tiff

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/fontconfig[>=2.6.0]
        media-libs/freetype:2[>=2.3.9]
        media-libs/libpng:=[>=1.2]
        net-dns/libidn
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        tiff? ( media-libs/tiff:=[>=3.8.2-r1] )
    run:
        !app-text/podofo:0[<0.9.8-r3] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    TIFF
)

podofo_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    # After merging the previously forked pdfmm back, tools are "currently
    # untested and unsupported". If that'll change -> PODOFO_BUILD_TOOLS
    #for binary in box color countpages crop encrypt gc imgextract img2pdf \
    #    impose incrementalupdates merge nooc pages pdfinfo sign txt2pdf \
    #    txtextract uncompress xmp ; do
    #    arch_dependent_alternatives+=(
    #        /usr/${host}/bin/${PN}${binary} ${PN}${binary}-${SLOT}
    #    )
    #done

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}             ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so           lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/lib${PN}.pc lib${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}


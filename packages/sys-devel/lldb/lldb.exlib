# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Copyright 2019 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project
if ever at_least 14 ; then
    require lua [ whitelist="5.3" with_opt=true multibuild=false ]
fi

SUMMARY="A next generation, high-performance debugger"

MYOPTIONS="
    doc
    libedit
    python
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/epydoc[python_abis:*(-)?]
            media-gfx/graphviz
        )
    build+run:
        dev-lang/clang:${LLVM_SLOT}[~${PV}]
        dev-lang/llvm:${LLVM_SLOT}[~${PV}]
        dev-libs/libxml2:2.0
        libedit? ( dev-libs/libedit )
        python? ( dev-lang/swig[>=3.0] )
"

if ever at_least 16 ; then
    :
else
    DEPENDENCIES+="
        build+run:
        dev-python/six[python_abis:*(-)?]
    "
fi

if ever at_least 14 ; then
    DEPENDENCIES+="
        build+run:
            app-arch/xz
            sys-libs/ncurses
            sys-libs/zlib
            lua? (
                dev-lang/swig[>=3.0]
            )
    "

    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DLLVM_ENABLE_ZLIB:BOOL=ON
        -DLLDB_ENABLE_LZMA:BOOL=ON
    )

    CMAKE_SRC_CONFIGURE_OPTIONS+=(
        'lua LLDB_ENABLE_LUA'
    )
fi

# Fails a lot of tests, upstream is aware of it being broken
# http://lists.llvm.org/pipermail/llvm-dev/2019-September/135114.html
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DClang_DIR:STRING="${LLVM_PREFIX}"/lib/cmake/clang

    -DLLDB_ENABLE_CURSES:BOOL=ON
    -DLLDB_USE_SYSTEM_SIX:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'doc LLDB_BUILD_DOCUMENTATION'
    'libedit LLDB_ENABLE_LIBEDIT'
    'python LLDB_ENABLE_PYTHON'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DLLDB_INCLUDE_TESTS:BOOL=TRUE -DLLDB_INCLUDE_TESTS:BOOL=FALSE'
)

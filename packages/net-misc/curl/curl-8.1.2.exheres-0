# Copyright 2008, 2009, 2010, 2011, 2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Based in part upon previous work copyrighted to Gentoo Foundation.
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A Client that groks URLs"
HOMEPAGE="https://${PN}.haxx.se"
DOWNLOADS="${HOMEPAGE}/download/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/changes.html"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ares [[ description = [ Prefer c-ares posix-threaded DNS resolver over curl's own threaded resolver ] ]]
    brotli [[ description = [ Brotli compression format support ] ]]
    http2
    hyper [[ description = [ An alternative HTTP backend utilizing net-libs/hyper ] ]]
    idn
    kerberos
    ldap
    rtmp
    zsh-completion
    zstd
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
    ( providers: gnutls libressl mbedtls openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-apps/diffutils
        virtual/pkg-config
    build+run:
        app-misc/ca-certificates
        net-libs/libssh2[>=1.2.8]
        sys-libs/zlib
        ares? ( net-dns/c-ares[>=1.6.0] )
        brotli? ( app-arch/brotli[>=0.6.0] )
        http2? ( net-libs/nghttp2[>=1.12.0] )
        hyper? ( net-libs/hyper )
        idn? ( net-dns/libidn2:= )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        ldap? ( net-directory/openldap )
        rtmp? ( media-video/rtmpdump )
        providers:gnutls? (
            dev-libs/gnutls[>=2.12.14]
            dev-libs/nettle:=
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl:= )
        zstd? ( app-arch/zstd )
    run:
        zsh-completion? ( app-shells/zsh )
"

DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( docs/ )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( BINDINGS DISTRO FEATURES INTERNALS MANUAL RESOURCES TheArtOfHttpScripting )

src_prepare() {
    # Reason: Binding to 0.0.0.0 creates access violations under sydbox.
    edo sed -e '/s_addr =/s:INADDR_ANY:htonl(INADDR_LOOPBACK):' -i tests/server/*.c

    # Use `whoami` instead of $USER;
    # sshserver.pl will refuse to run as root (which $USER might claim ever if we're not)
    edo sed -e 's/\$ENV{USER}/`whoami`/' -i tests/*.pl

    default
}

src_configure() {
    econf_params=(
        --enable-cookies
        --enable-crypto-auth
        --enable-dateparse
        --enable-dnsshuffle
        --enable-doh
        --enable-get-easy-options
        --enable-headers-api
        --enable-http-auth
        --enable-largefile
        --enable-manual
        --enable-mime
        --enable-netrc
        --enable-progress-meter
        --enable-proxy
        --enable-pthreads
        --enable-rt
        --enable-socketpair
        --enable-threaded-resolver
        --enable-tls-srp
        --enable-unix-sockets
        --enable-{dict,file,ftp,gopher,http,ipv6,imap,pop3,rtsp,smb,smtp,telnet,tftp}
        --disable-alt-svc
        --disable-hsts
        --disable-mqtt
        --disable-ntlm
        --disable-sspi
        --disable-static
        --disable-werror
        --with-libssh2
        --with-ssl
        --with-zlib
        --without-amissl
        --without-bearssl
        --without-ca-bundle
        # Would test again an apache/caddy server
        --without-test-caddy
        --without-test-httpd
        --without-libgsasl
        --without-libpsl
        --without-libssh
        --without-mbedtls
        --without-msh3
        --without-nghttp3
        --without-ngtcp2
        --without-nss
        --without-quiche
        --without-rustls
        --without-schannel
        --without-secure-transport
        --without-wolfssl
        --without-default-ssl-backend
        --without-fish-functions-dir
    )

    econf_option_enables=(
        ares
        ldap
        'ldap ldaps'
    )

    econf_option_withs=(
        brotli
        'http2 nghttp2'
        hyper
        'idn libidn2'
        'kerberos gssapi'
        'rtmp librtmp'
        'zsh-completion zsh-functions-dir /usr/share/zsh/site-functions'
        zstd
    )

    for opt in providers:{gnutls,libressl,mbedtls,openssl};do
        optionq "${opt}" && ssl_provider="${opt##*:}" && break
    done

    case "${ssl_provider}" in
        gnutls)
            econf_params+=(
                --disable-ech
                --with-gnutls --without-mbedtls --without-openssl
                --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt
            )
        ;;
        mbedtls)
            econf_params+=(
                --disable-ech
                --with-mbedtls --without-gnutls --without-openssl
                --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt
            )
        ;;
        libressl|openssl)
            econf_params+=(
                # https://github.com/openssl/openssl/issues/7482
                --disable-ech
                --enable-openssl-auto-load-config
                --with-openssl --without-mbedtls --without-gnutls
                --with-ca-path=/etc/ssl/certs
            )
        ;;
    esac

    econf \
        "${econf_params[@]}"    \
        $(for opt in "${econf_option_enables[@]}"; do
            option_enable ${opt}
        done) \
        $(for opt in "${econf_option_withs[@]}"; do
            option_with ${opt}
        done)
}

src_test() {
    cat <<-EOF >> tests/data/DISABLED
# Work around broken DNS servers, Exherbo #207
20
507
# Fails with too many open files
537
# Can't open perl script "./manpage-scan.pl": No such file or directory
1139
1140
EOF
    esandbox allow_net --connect "inet:127.0.0.1@9011"
    esandbox allow_net --connect "inet:127.0.0.1@60000"
    esandbox allow_net --connect "inet6:::1@8999"

    # Default TEST_F is -a -p -r; we add -n (no valgrind) -v (verbose) and remove -r (run time statistics)
    local test_args=( -a -p -n -v )
    # SCP/SFTP tests hang/fail
    test_args+=( '!'{582,{600..699}} )
    # Fail (last checked: 7.41.0)
    test_args+=( '!'1135 )
    # Fail (last checked: 7.50.2)
    test_args+=( '!'2032 )
    # Fail (last checked: 7.51.0)
    test_args+=( '!'{165,1034,1035,2046,2047} )
    # Fail (last checked: 7.54.1)
    test_args+=( '!'1446 )
    # Hang/Fail (last checked 7.55.1)
    test_args+=( '!'{1061,1119,1148} )
    # Fail (last checked: 7.56.0)
    test_args+=( '!'1453 )
    # Wants to use 0.0.0.0, last checked 7.60.0
    test_args+=( '!'1455 )
    # Fail (last checked: 7.72.0)
    test_args+=( '!'{320,321,322,324} )
    # Hang with hyper backend (last checked: 7.77.0)
    # Two are HTTP proxy digest related (206, 1060)
    # The rest are related to transfer-encodings (gzip, etc.)
    optionq hyper && test_args+=( '!'{206,319,1060,1122,1123,1125,1170,1171,1416} )
    # Fail with hyper backend (last checked: 7.77.0)
    if optionq hyper; then
        test_args+=( '!'{94,129,154,158,176,178,207,217,221,222,223,224,230,232,233}
            '!'{246,257,262,266,269,281,287,326,328,339,347,357,393,394,395,396,397}
            '!'{493,500,503,508,510,512,513,514,515,516,518,519,540,543,544,545,551}
            '!'{552,553,554,559,565,566,573,576,578,579,580,581,584,585,587,589,598}
            '!'{599,718,941,1001,1002,1013,1030,1033,1051,1052,1055,1070,1071,1074}
            '!'{1075,1077,1078,1087,1114,1115,1116,1124,1129,1130,1131,1138,1144,1147}
            '!'{1151,1154,1156,1160,1164,1172,1174,1179,1216,1218,1230,1266,1267,1268}
            '!'{1287,1288,1417,1428,1429,1430,1431,1432,1433,1456,1461,1462,1463,1464}
            '!'{1500,1502,1503,1504,1505,1509,1511,1514,1517,1518,1519,1522,1525,1526}
            '!'{1527,1528,1533,1538,1540,1554,1556,1558,1568,1591,1594,1595,1596,1800}
            '!'{2023,2024,2025,2026,2027,2028,2029,2030,2031,2058,2059,2060,3010}
        )
    fi
    # Fail (last checked: 7.80.0)
    test_args+=( '!'{1459,3021,3022} )

    emake -j1 test-full TEST_F="${test_args[*]}"

    esandbox disallow_net --connect "inet:127.0.0.1@9011"
    esandbox disallow_net --connect "inet:127.0.0.1@60000"
    esandbox disallow_net --connect "inet6:::1@8999"
}

src_install() {
    default

    # curl installs zsh completion file to /_curl if disabled!
    if [[ -f "${IMAGE}"/_curl ]] && ! option zsh-completion;then
        edo rm -f "${IMAGE}"/_curl
    fi

    insinto /usr/share/aclocal
    doins "${WORK}"/docs/libcurl/libcurl.m4
}


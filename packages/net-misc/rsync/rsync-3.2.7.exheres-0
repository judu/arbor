# Copyright 2007 Bryan Østergaard
# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require systemd-service [ systemd_files=[ packaging/systemd/rsync{.service,.socket,@.service} ] ]

SUMMARY="Rsync provides fast incremental file transfers"
HOMEPAGE="https://rsync.samba.org"
DOWNLOADS="${HOMEPAGE}/ftp/${PN}/src/${PNV}.tar.gz"

UPSTREAM_CHANGELOG="${DOWNLOADS/.tar.gz/-NEWS}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    acl
    lz4 [[ description = [ Support for the LZ4 compression algorithm ] ]]
    xattr
    zstd
    parts: binaries documentation
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        dev-libs/popt
        dev-libs/xxHash [[ note = [ optional, but tiny and apparently fast ] ]]
        sys-libs/zlib
        acl? ( sys-apps/acl )
        lz4? ( app-arch/lz4 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        xattr? ( sys-apps/attr )
        zstd? ( app-arch/zstd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-ipv6
    --enable-openssl
    --enable-xxhash
    --disable-md2man
    --disable-roll-asm
    --disable-roll-simd
    --with-included-zlib=no
    --without-included-popt
    # Script to setup restricted rsync users, would need py3 and braceexpand
    # (optional, but suggested).
    --without-rrsync
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'acl acl-support'
    lz4
    'xattr xattr-support'
    zstd
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( OLDNEWS )

src_install() {
    default

    install_systemd_files

    expart binaries /usr/$(exhost --target)/bin
    expart documentation /usr/share/{doc,man}
}


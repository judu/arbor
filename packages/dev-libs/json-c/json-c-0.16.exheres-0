# Copyright 2011 Paul Seidler <sepek@exherbo.org>
# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github cmake
require alternatives

SUMMARY="Reference counting object model that lets you construct JSON objects in C"
HOMEPAGE="https://github.com/${PN}/${PN}/wiki"
DOWNLOADS="https://s3.amazonaws.com/${PN}_releases/releases/${PNV}.tar.gz"

LICENCES="MIT"
# SOVERSION in CMakeLists.txt
SLOT="5"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/libbsd
    run:
        !dev-libs/json-c:0[<0.13.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
    -DENABLE_RDRAND:BOOL=TRUE
    -DENABLE_THREADING:BOOL=TRUE
    -DDISABLE_EXTRA_LIBS:BOOL=FALSE
    -DDISABLE_JSON_POINTER:BOOL=FALSE
    -DDISABLE_WERROR:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( 'doc Doxygen' )

src_compile() {
    cmake_src_compile

    option doc && emake doc
}

src_install() {
    local local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    if option doc ; then
        docinto html
        dodoc -r doc/html/*
    fi

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

pkg_preinst() {
    if has_version dev-libs/json-c ; then
        # Older versions installed to /usr/include/json, 0.11 installs a symlink
        [[ -d "${ROOT}"/usr/include/json ]] && edo rm -r "${ROOT}"/usr/include/json
    fi
}


# Copyright 2007 Bryan Østergaard
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] alternatives

SUMMARY="Grep searches input files for matches to specified patterns"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( linguas: af be bg ca cs da de el eo es et eu fi fr ga gl he hr hu id it ja ko ky lt nb nl pa
               pl pt pt_BR ro ru sk sl sr sv th tr uk vi zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-devel/gettext[>=0.18.2]
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/pcre2
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-nls
    --enable-perl-regexp
    --program-prefix=g
    gt_cv_func_gnugettext{1,2}_libc=yes
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README-alpha )

src_test() {
    # required for the bundled gnulib tests
    esandbox allow_net --bind "inet:127.0.0.1@80"
    esandbox allow_net --connect "inet:127.0.0.1@80"

    # tests use installed grep without this
    PATH="${WORK}/src:${PATH}" default

    esandbox disallow_net --connect "inet:127.0.0.1@80"
    esandbox disallow_net --bind "inet:127.0.0.1@80"
}

src_install() {
    default

    nonfatal edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/charset.alias && nonfatal edo rmdir "${IMAGE}"/usr/$(exhost --target)/lib

    alternatives_for grep gnu 1000  \
        /usr/$(exhost --target)/bin/grep    ggrep       \
        /usr/$(exhost --target)/bin/egrep   gegrep      \
        /usr/$(exhost --target)/bin/fgrep   gfgrep      \
        /usr/share/man/man1/grep.1          ggrep.1
}


# Copyright 2009, 2010, 2011, 2012, 2013, 2021 Ali Polatel <alip@exherbo.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.exherbo.org" user="exherbo-misc" pn="${PN}-1" tag="v${PV}" new_download_scheme=true ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
export_exlib_phases src_test pkg_postinst

SUMMARY="Sydbox, the other sandbox"
DESCRIPTION="Sydbox is a ptrace based sandbox for Linux."

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""
DEPENDENCIES=""

REMOTE_IDS="freecode:${PN}"

if ever at_least 2; then
    HOMEPAGE="https://sydbox.exherbolinux.org"

    MYOPTIONS+="debug"
    DEPENDENCIES+="
    build:
        sys-kernel/linux-headers[>=5.6]
        debug? ( dev-libs/libunwind )
    test:
        net-dns/bind-tools
    "
elif ever at_least 1; then

    HOMEPAGE="https://gitlab.exherbo.org/exherbo-misc/sydbox-1.git"

    # sydbox 1(_pre): has seccomp
    MYOPTIONS+="
    debug
    seccomp [[ description = [ Enable seccomp user filter support ] ]]"
    DEPENDENCIES+="
    build:
        debug? ( dev-libs/libunwind )
        seccomp? ( sys-kernel/linux-headers[>=3.5] )"

    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug seccomp )

    if ever at_least 1.0.0; then
        # sydbox 1: external pinktrace
        DEPENDENCIES+="
        build+run:
            dev-libs/pinktrace[>=0.9.0]"
    else
        # sydbox 1_pre: internal pinktrace
        MYOPTIONS+="
        doc [[ description = [ Build API documentation of included pinktrace library ] ]]"
        DEPENDENCIES+="
        build:
            doc? ( app-doc/doxygen )"

        DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-ipv6 )
        DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=( "doc doxygen" )
    fi

else

    HOMEPAGE="https://gitlab.exherbo.org/exherbo-misc/sydbox.git"

    # sydbox 0: no seccomp, glib, external pinktrace
    DEPENDENCIES+="
    build+run:
        dev-libs/glib:2[>=2.18]
        dev-libs/pinktrace[>=0.1.2]"
fi

if ever at_least 2; then
    MYOPTIONS+="
    static
"
    DEFAULT_SRC_CONFIGURE_OPTION_ENABLES+=( static )
fi

sydbox_src_test() {
    if ! esandbox check 2>/dev/null; then
        default
    else
        elog "Not running tests because sydbox doesn't work under sydbox"
        elog "set PALUDIS_DO_NOTHING_SANDBOXY=1 if you want to run the tests"

        if ever at_least 1.0.2; then
            elog "As of sydbox-1.0.2, tests are installed by default."
            elog "You can use the helper utility sydtest to run the tests."
        fi
    fi
}

sydbox_pkg_postinst() {
    if ever at_least 2; then
        elog "SydBox-2 does not use ptrace() and uses seccomp notify."
        elog "You may safely uninstall PinkTrace."
        elog
        elog "Paludis does not support SydBox API 2 yet!"
        elog "See this Merge Request for current progress:"
        elog "https://gitlab.exherbo.org/paludis/paludis/-/merge_requests/36"
        elog
        elog "You need a recent Linux kernel, 5.6 or newer is required."
        elog "Use \`sydbox --test' to verify SydBox-2 is supported on your system."
        elog "Use the helper utility \`syd-test' to verify SydBox-2 works on your system."
        elog "Use a non-privileged (not root!) user when running tests!"
        elog
        elog "Report any bugs to alip@exherbo.org"
        elog "You may also use https://todo.sr.ht/~alip/sydbox"
        elog "Attaching poems encourages consideration tremendously."
   fi

   default
}

# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012, 2013 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'iproute2-2.6.26-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require toolchain-funcs

export_exlib_phases src_prepare src_configure src_install

myexparam headers_version="[>=$(ever range -2)]"

SUMMARY="Collection of utilities for controlling networking and traffic control in Linux"
DESCRIPTION="
A replacement for the inadequately behaving ifconfig/route programs in modern networks.
"
HOMEPAGE="https://wiki.linuxfoundation.org/networking/${PN}"
DOWNLOADS="mirror://kernel/linux/utils/net/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    atm [[ description = [ Add ATM support in tc program ] ]]
    berkdb [[ description = [ Build ARP daemon ] ]]

    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
        sys-kernel/linux-headers$(exparam headers_version)
        virtual/pkg-config
    build+run:
        dev-libs/libbsd [[ note = [ automagic ] ]]
        net-libs/libmnl
        net-firewall/iptables[>=1.4.5]
        sys-libs/libcap
        atm? ( net-dialup/linux-atm )
        berkdb? ( sys-libs/db:= )
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf )
"

if ever at_least 5.18.0 ; then
    MYOPTIONS+="
        rpc [[ description = [ Support for getting information about rpc services sockets ] ]]
    "
    DEPENDENCIES+="
        build+run:
            rpc? ( net-libs/libtirpc )
    "
fi

DEFAULT_SRC_COMPILE_PARAMS+=(
    CC="${CC}"
    HOSTCC="$(exhost --build)-cc"
    AR="${AR}"
    HDRDIR=/usr/$(exhost --target)/include/${PN}
    KERNEL_INCLUDE=/usr/$(exhost --target)/include
    LIBDIR=/usr/$(exhost --target)/lib
    IPT_LIB_DIR=/usr/$(exhost --target)/lib
    NETNS_RUN_DIR=/run/netns
    V=1
)

DEFAULT_SRC_INSTALL_PARAMS+=(
    SBINDIR=/usr/$(exhost --target)/bin
    LIBDIR=/usr/$(exhost --target)/lib
    HDRDIR=/usr/$(exhost --target)/include/${PN}
    DOCDIR=/usr/share/doc/${PNVR}
    MANDIR=/usr/share/man
)

iproute2_src_prepare() {
    # apply version-specific patches
    default

    # use system headers
    if [[ "$(exhost --target)" != *-musl* ]]; then
        edo rm -rf include/netinet
    fi

    # honor user CFLAGS, nuke -Werror
    edo sed -e "/^CCOPTS =/s|-O2|${CFLAGS}|" \
            -e "s/-Werror//" -i Makefile

    # use /run instead of /var/run
    edo sed \
        -e 's:/var/run:/run:g' \
        -i include/namespace.h

    if ! ever at_least 5.7.0 ; then
        edo sed \
            -e 's:/var/run:/run:g' \
            -i man/man8/ip-netns.8
    fi
}

iproute2_src_configure() {
    if ever at_least 5.11.0 && ! ever at_least 5.14.0 ; then
        edo export LIBBPF_FORCE=off
    fi

    local myconf=(
        --libbpf_force off
    )

    if ever at_least 5.16.0 ; then
        myconf+=(
            --include_dir=/usr/$(exhost --target)/include
            --prefix=/usr/$(exhost --target)
            --libdir=/usr/$(exhost --target)/lib
        )
    fi

    if ever at_least 5.14.0 ; then
        edo ./configure "${myconf[@]}"
    else
        edo ./configure
    fi

    local CONFIG=config.mk

    edo echo 'HAVE_SELINUX:=n' >> ${CONFIG}
    edo echo 'HAVE_MNL:=y' >> ${CONFIG}

    if option atm ; then
        edo echo 'TC_CONFIG_ATM:=y' >> ${CONFIG}
    else
        edo echo 'TC_CONFIG_ATM:=n' >> ${CONFIG}
    fi

    if option berkdb ; then
        edo echo 'HAVE_BERKELEY_DB:=y' >> ${CONFIG}
    else
        edo echo 'HAVE_BERKELEY_DB:=n' >> ${CONFIG}
    fi

    if ever at_least 5.18.0 ; then
        if option rpc ; then
            edo echo 'HAVE_RPC:=y' >> ${CONFIG}
        else
            edo echo 'HAVE_RPC:=n' >> ${CONFIG}
        fi
    fi
}

iproute2_src_install() {
    default

    if optionq berkdb ; then
        keepdir /var/lib/arpd
    else
        edo rmdir "${IMAGE}"/var/{lib/{arpd,},}
    fi
}

